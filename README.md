# rapyuta_14.04 

This package contains rapyuta package and installation instructions for ubuntu 14.04

## Prerequisite 

For complete setup following are the prerequisite

### System Requirement
#### Operating System
One system running **ubuntu 14.04**.

### Dependencies
#### Packages and Library 

* Install the following packages.

```bash
 $ sudo apt-get install python-setuptools python-dev git-core
```
* For 'xtwrapper' library clone and build 'python-iptables'.

```bash
 $ git clone -b master https://github.com/ldx/python-iptables.git python-iptables
 $ cd python-iptables/
 $ sudo python setup.py install_lib
```

### Cloning and installating the package.

* Clone the package 'rapyuta_14.04'

```bash
 $ cd
 $ git clone git@gitlab.com:prasun2712/rce.git
 $ cd ~/rce
 $ sudo ./install.sh
 $ ./setup/provision all
```
 
### Terminal inputs to be given :

```bash
 a) Do you want to auto-provision/recreate credentials database for developer mode (Insecure) [y/N]: y <Enter>
 b) Enter the root directory to store the Rapyuta Container Filesystem [/opt/rce/tainer]: <Enter>
 c) Enter the Ubuntu release to deploy in the container.
    0 -> precise (12.04 LTS)
    1 -> quantal (12.10)
    2 -> raring (13.04)
    3 -> saucy (13.10)
    4 -> trusty (14.04 LTS)
    Selection [4]: 4 <Enter>
 d) Enter the ROS release to deploy in the container.
    0 -> fuerte
    1 -> groovy
    2 -> hydro
    3 -> indigo
    Selection [3]: 3 <Enter>
 e) Enter the deployment platform for easier auto configuration.
    0 -> aws
    1 -> rackspace
    2 -> other
    Selection [2]: 2 <Enter>
 f) Enter the interface name or IP address for the external communication [eth0]: wlan0 <Enter>
 g) Enter the interface name or IP address for the internal communication [eth0]: wlan0 <Enter>
 h) Enter the interface name or IP address for the container communication [lxcbr0]: lxcbr0 <Enter>
```
 After getting all the inputs the installation and setup process continues. Once it is done.<br />
 <br />
```bash 
  $ sudo ./final.sh
```
### Installation done successfully.