#!/bin/bash

if [ -d /opt/rce/container/rootfs/sys ] ; then

sudo mkdir -p /opt/rce/container/rootfs/sys/fs/pstore

echo " "
echo " "
echo " "
echo -en '\E[47;34m'"\033[1m*** INSTALLATION DONE ***\033[0m"
echo;
echo -en '\E[47;34m'"\033[1m******* THANK YOU *******\033[0m"
echo;
echo " "
echo " "
echo " "
tput sgr0

else

echo " "
echo " "
echo " "
echo -en '\E[47;31m'"\033[1m****** INSTALLATION FAILED *******\033[0m"
echo;
echo -en '\E[47;31m'"\033[1m******* PLEASE CHECK AGAIN *******\033[0m"
echo;
echo " "
echo " "
echo " "
tput sgr0

fi